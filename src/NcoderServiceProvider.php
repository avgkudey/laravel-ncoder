<?php


namespace Tetracode\Ncoder;


use Illuminate\Support\ServiceProvider;

class NcoderServiceProvider extends ServiceProvider {

    public function boot() {
        $this->loadRoutesFrom(__DIR__.'/routes/web.php');
        $this->loadViewsFrom(__DIR__.'/resources/views', 'ncoder');
        $this->mergeConfigFrom(
            __DIR__.'/config/ncoder.php', 'ncoder'
        );





        $this->publishes([
            __DIR__.'/config/ncoder.php' => config_path('ncoder.php'),
            __DIR__.'/resources/views' => resource_path('views/vendor/ncoder'),
        ]);
    }

    public function register() {
    }

}
